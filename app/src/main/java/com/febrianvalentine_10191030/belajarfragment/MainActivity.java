package com.febrianvalentine_10191030.belajarfragment;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.widget.Toolbar;

import com.google.android.material.tabs.TabLayout;

import adapter.TabFragmentPagerAdapter;

public class MainActivity extends AppCompatActivity {
    //deklarasi semua komponen View yang akan digunakan
    private Toolbar toolbar;
    private ViewPager pager;
    private TabLayout tabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //set up toolbar
        toolbar = (Toolbar)findViewById(R.id.tool_bar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Material Tab");
            pager=(ViewPager)findViewById(R.id.pager);
            tabs=(TabLayout)findViewById(R.id.tabs);

            //set object adapter kedalam ViewPager
            pager.setAdapter(new TabFragmentPagerAdapter(getSupportFragmentManager()));

            //Manipulasi sedikit  untuk set TextColor pada Tab
            tabs.setTabTextColors(getResources().getColor(R.color.design_default_color_primary_dark),
                getResources().getColor(android.R.color.white));

            //set tab ke ViewPager
            tabs.setupWithViewPager(pager);

            //konfigurasi Gravity Fill untuk Tab berada di posisi yang proposional
            tabs.setTabGravity(TabLayout.GRAVITY_FILL);

    }
    private void setSupportActionBar(Toolbar toolbar) {

    }
}